# cryptocoin-address
[![License](http://img.shields.io/badge/license-GPLV3-blue.svg)](https://gitlab.com/Journeyman/cryptocoin-address/-/blob/main/LICENSE)
[![pipeline status](https://gitlab.com/Journeyman/cryptocoin-address/badges/main/pipeline.svg)](https://gitlab.com/Journeyman/cryptocoin-address/-/commits/main)

## Description
A Rust crate for generating and accquiring cryptocoin addresses.

## Installation
```shell
cargo install cryptocoin-address --git https://gitlab.com/Journeyman/cryptocoin-address
```
## Requirements

- Rust (`cargo`, `rustc`)

## Contributing
Use the issue tracker to report problems, suggestions and questions. You may also contribute by submitting pull requests.

If you find this project helpful, please consider making a donation to my coffee fund: `bc1qq3u0pyvdk3zaj3aut0emqv3edaqwam2n2v9f03`

## Copyright
Copyright (C) 2024 Chris Morrison

## License
See [LICENSE](https://gitlab.com/Journeyman/cryptocoin-address/-/blob/main/LICENSE)

