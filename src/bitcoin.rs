/* Copyright (C) 2023-2024 Chris Morrison
 *
 * This file is part of cryptocoin-address.
 *
 * cryptocoin-address is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cryptocoin-address is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cryptocoin-address. If not, see <https://www.gnu.org/licenses/>.
 */

use base58::{FromBase58, ToBase58};
use base64::{engine::general_purpose, Engine as _};
use bitcoin::address::Payload;
use bitcoin::secp256k1::{Secp256k1, SecretKey};
use bitcoin::{Address, Network, PrivateKey};
use hex::FromHex;
use num_bigint::BigUint;
use regex::Regex;
use sha256_rs::sha256;
use std::fmt::{Debug, Display, Formatter};
use std::str::FromStr;

use crate::{build_bitcoin_descriptor, BitKeyWalletAddress, CryptocoinAddress};

// -----------------------------------------------------------------------------------------------
// Legacy Bitcoin address
// -----------------------------------------------------------------------------------------------

#[derive(Clone)]
pub struct LegacyBitcoinAddress
{
    address: String,
    public_key_hash: String,
    wif_private_key: String,
    mini_key: String,
    descriptor: String,
    compressed: bool
}

impl BitKeyWalletAddress for LegacyBitcoinAddress
{
    fn from_wif_private_key(wif: &str) -> Result<LegacyBitcoinAddress, Box<dyn std::error::Error>>
    {
        let secp = Secp256k1::new();
        let privk = PrivateKey::from_wif(&wif)?;
        let pubkhash = privk.public_key(&secp).pubkey_hash();

        return Ok(LegacyBitcoinAddress
        {
            address: Address::new(Network::Bitcoin, Payload::PubkeyHash(pubkhash)).to_string(),
            public_key_hash: pubkhash.to_string(),
            wif_private_key: privk.to_wif(),
            mini_key: String::default(),
            descriptor: build_bitcoin_descriptor("p2pkh", privk.to_wif().as_str())?,
            compressed: privk.compressed,
        });
    }

    fn wif_private_key(&self) -> &str
    {
        return self.wif_private_key.as_str();
    }

    fn from_base2_private_key(raw_key: &str, compressed: bool) -> Result<LegacyBitcoinAddress, Box<dyn std::error::Error>>
    {
        let regex = Regex::new(r"[01]{8}")?;
        let octets: Vec<&str> = regex.find_iter(raw_key).map(|m| m.as_str()).collect();
        if octets.len() > 32
        {
            return Err("Invalid base2 binary key! The key cannot be longer than 32 bytes.")?;
        }

        let mut bytes: [u8; 32] = [0; 32];
        for (i, bs) in octets.iter().enumerate()
        {
            if let Some(bv) = crate::bin2octet(bs)
            {
                bytes[i] = bv;
            }
            else
            {
                return Err("Invalid base2 value!")?;
            }
        }

        return Self::from_slice(&bytes, compressed);
    }

    fn from_base16_private_key(raw_key: &str, compressed: bool) -> Result<LegacyBitcoinAddress, Box<dyn std::error::Error>>
    {
        let hex_str: String;
        if raw_key.len() > 64
        {
            return Err("Invalid hexadecimal private key! The key cannot be longer than 64 characters (32 bytes).")?;
        }
        else if raw_key.len() == 64
        {
            hex_str = String::from(raw_key);
        }
        else
        {
            hex_str = format!("{:0<64}", raw_key);
        }

        let bytes = <[u8; 32]>::from_hex(hex_str.as_str())?;

        return Self::from_slice(&bytes, compressed);
    }

    fn from_base58_private_key(raw_key: &str, compressed: bool) -> Result<LegacyBitcoinAddress, Box<dyn std::error::Error>>
    {
        if raw_key.len() != 44
        {
            panic!("Invalid base58 private key! The encoded key string must be 44 characters long.");
        }

        return match raw_key.from_base58()
        {
            Ok(bytes) =>
                {
                    if bytes.len() != 32
                    {
                        Err(format!("Decoded data should be 32 bytes, not {}", bytes.len()))?
                    }

                    Self::from_slice(&bytes, compressed)
                }
            Err(e) =>
                {
                    Err(format!("Error decoding base58: {:?}", e))?
                }
        }
    }

    fn from_base64_private_key(raw_key: &str, compressed: bool) -> Result<LegacyBitcoinAddress, Box<dyn std::error::Error>>
    {
        if raw_key.len() != 44
        {
            return Err("Invalid base64 private key! The encoded key string must be 44 characters long.")?;
        }

        let bytes = general_purpose::STANDARD.decode(raw_key)?;

        if bytes.len() != 32
        {
            return Err(format!("Decoded array should be 32 bytes, not {}", bytes.len()))?;
        }

        return Self::from_slice(&bytes, compressed);
    }

    fn from_big_uint(key: &BigUint, compressed: bool) -> Result<LegacyBitcoinAddress, Box<dyn std::error::Error>>
    {
        let secp = Secp256k1::new();
        let priv_key_str = format!("{:0>64}", key.to_str_radix(16));
        let sk = SecretKey::from_str(&priv_key_str)?;
        let privk = match compressed
        {
            true => PrivateKey::new(sk, Network::Bitcoin),
            false => PrivateKey::new_uncompressed(sk, Network::Bitcoin),
        };

        let pubkhash = privk.public_key(&secp).pubkey_hash();

        return Ok(LegacyBitcoinAddress
        {
            address: Address::new(Network::Bitcoin, Payload::PubkeyHash(pubkhash)).to_string(),
            public_key_hash: pubkhash.to_string(),
            wif_private_key: privk.to_wif(),
            mini_key: String::new(),
            descriptor: build_bitcoin_descriptor("p2pkh", privk.to_wif().as_str())?,
            compressed,
        });
    }

    fn from_slice(data: &[u8], compressed: bool) -> Result<LegacyBitcoinAddress, Box<dyn std::error::Error>>
    {
        let secp = Secp256k1::new();
        let sk = SecretKey::from_slice(data)?;
        let privk = match compressed
        {
            true => PrivateKey::new(sk, Network::Bitcoin),
            false => PrivateKey::new_uncompressed(sk, Network::Bitcoin),
        };

        let pubkhash = privk.public_key(&secp).pubkey_hash();

        return Ok(LegacyBitcoinAddress
        {
            address: Address::new(Network::Bitcoin, Payload::PubkeyHash(pubkhash)).to_string(),
            public_key_hash: pubkhash.to_string(),
            wif_private_key: privk.to_wif(),
            mini_key: String::new(),
            descriptor: build_bitcoin_descriptor("p2pkh", privk.to_wif().as_str())?,
            compressed,
        });
    }
}

impl CryptocoinAddress for LegacyBitcoinAddress
{
    fn descriptor(&self) -> &str
    {
        return self.descriptor.as_str();
    }
}

impl PartialEq<Self> for LegacyBitcoinAddress
{
    fn eq(&self, other: &Self) -> bool
    {
        return self.address.eq(&other.address);
    }
}

impl PartialEq<&str> for LegacyBitcoinAddress
{
    fn eq(&self, other: &&str) -> bool
    {
        return self.address.as_str().eq(*other);
    }
}

impl PartialEq<String> for LegacyBitcoinAddress
{
    fn eq(&self, other: &String) -> bool
    {
        return self.address.eq(other);
    }
}

impl Eq for LegacyBitcoinAddress
{
    
}

impl LegacyBitcoinAddress
{
    fn from_private_key(key: &PrivateKey) -> Self
    {
        let secp = Secp256k1::new();
        let pubkhash = key.public_key(&secp).pubkey_hash();

        return LegacyBitcoinAddress
        {
            address: Address::new(Network::Bitcoin, Payload::PubkeyHash(pubkhash)).to_string(),
            public_key_hash: pubkhash.to_string(),
            wif_private_key: key.to_wif(),
            mini_key: String::default(),
            descriptor: build_bitcoin_descriptor("p2pkh", key.to_wif().as_str()).unwrap(),
            compressed: key.compressed,
        }
    }

    pub fn from_mini_private_key(mini_key: &str, compressed: bool) -> Result<LegacyBitcoinAddress, Box<dyn std::error::Error>>
    {
        return if let Some(pk) = is_valid_mini_key(mini_key)
        {
            Ok(LegacyBitcoinAddress { mini_key: String::from(mini_key), ..Self::from_slice(&pk, compressed)? })
        }
        else
        {
            Err("Invalid mini key!")?
        }
    }
    
    pub fn mini_key(&self) -> Option<&str>
    {
        if !self.mini_key.is_empty()
        {
            return Some(&self.mini_key);
        }
        return None;
    }

    pub fn public_key_hash(&self) -> &str
    {
        return &self.public_key_hash;
    }
    
    pub fn is_compressed(&self) -> bool
    {
        return self.compressed;
    }
}

impl Display for LegacyBitcoinAddress
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result
    {
        return write!(f, "{}", self.address);
    }
}

impl Debug for LegacyBitcoinAddress
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result
    {
        return write!(f, "Address = {}, WIF private key = {}", self.address, self.wif_private_key);
    }
}

impl<Idx> std::ops::Index<Idx> for LegacyBitcoinAddress where Idx: std::slice::SliceIndex<str>,
{
    type Output = Idx::Output;

    fn index(&self, index: Idx) -> &Self::Output
    {
        return &self.address.as_str()[index];
    }
}

impl AsRef<str> for LegacyBitcoinAddress
{
    fn as_ref(&self) -> &str
    {
        return self.address.as_str();
    }
}

impl AsRef<[u8]> for LegacyBitcoinAddress
{
    fn as_ref(&self) -> &[u8]
    {
        return self.address.as_bytes();
    }
}

#[derive(Clone)]
pub struct LegacyBitcoinAddressPair
{
    compressed: LegacyBitcoinAddress,
    uncompressed: LegacyBitcoinAddress,
}

impl PartialEq<Self> for LegacyBitcoinAddressPair
{
    fn eq(&self, other: &Self) -> bool
    {
        return self.compressed.eq(&other.compressed) && self.uncompressed.eq(&other.uncompressed);
    }
}

impl PartialEq<&str> for LegacyBitcoinAddressPair
{
    fn eq(&self, other: &&str) -> bool
    {
        if self.compressed.address.eq(other)
        {
            return true;
        }
        if self.uncompressed.address.eq(other)
        {
            return true;
        }
        
        return false;
    }
}

impl PartialEq<String> for LegacyBitcoinAddressPair
{
    fn eq(&self, other: &String) -> bool
    {
        if self.compressed.address.eq(other)
        {
            return true;
        }
        if self.uncompressed.address.eq(other)
        {
            return true;
        }

        return false;
    }
}

impl Eq for LegacyBitcoinAddressPair
{

}

impl LegacyBitcoinAddressPair
{
    fn from_private_key(key: &PrivateKey) -> Self
    {
        let mut privkc= key.clone();
        privkc.compressed = true;
        let mut privku= key.clone();
        privku.compressed = false;

        return LegacyBitcoinAddressPair
        {
            compressed: LegacyBitcoinAddress::from_private_key(&privkc),
            uncompressed: LegacyBitcoinAddress::from_private_key(&privku),
        }
    }

    pub fn from_wif_private_key(wif: &str) -> Result<LegacyBitcoinAddressPair, Box<dyn std::error::Error>>
    {
        let mut privku = PrivateKey::from_wif(&wif)?;
        privku.compressed = false;
        let mut privkc = PrivateKey::from_wif(&wif)?;
        privkc.compressed = true;

        return Ok(LegacyBitcoinAddressPair
        {
            compressed: LegacyBitcoinAddress::from_private_key(&privkc),
            uncompressed: LegacyBitcoinAddress::from_private_key(&privku),
        });
    }

    pub fn from_big_uint(key: &BigUint) -> Result<LegacyBitcoinAddressPair, Box<dyn std::error::Error>>
    {
        return Ok(LegacyBitcoinAddressPair
        {
            compressed: LegacyBitcoinAddress::from_big_uint(&key, true)?,
            uncompressed: LegacyBitcoinAddress::from_big_uint(&key, false)?,
        });
    }
    
    pub fn from_base2_private_key(raw_key: &str) -> Result<LegacyBitcoinAddressPair, Box<dyn std::error::Error>>
    {
        let regex = Regex::new(r"[01]{8}")?;
        let octets: Vec<&str> = regex.find_iter(raw_key).map(|m| m.as_str()).collect();
        if octets.len() > 32
        {
            return Err("Invalid base2 binary key! The key cannot be longer than 32 bytes.")?;
        }

        let mut bytes: [u8; 32] = [0; 32];
        for (i, bs) in octets.iter().enumerate()
        {
            if let Some(bv) = crate::bin2octet(bs)
            {
                bytes[i] = bv;
            }
            else
            {
                return Err("Invalid base2 value!")?;
            }
        }

        return Self::from_slice(&bytes);
    }

    pub fn from_base16_private_key(raw_key: &str) -> Result<LegacyBitcoinAddressPair, Box<dyn std::error::Error>>
    {
        let hex_str: String;
        if raw_key.len() > 64
        {
            return Err("Invalid hexadecimal private key! The key cannot be longer than 64 characters (32 bytes).")?;
        }
        else if raw_key.len() == 64
        {
            hex_str = String::from(raw_key);
        }
        else
        {
            hex_str = format!("{:0<64}", raw_key);
        }

        let bytes = <[u8; 32]>::from_hex(hex_str.as_str())?;

        return Self::from_slice(&bytes);
    }

    pub fn from_base58_private_key(raw_key: &str) -> Result<LegacyBitcoinAddressPair, Box<dyn std::error::Error>>
    {
        if raw_key.len() != 44
        {
            panic!("Invalid base58 private key! The encoded key string must be 44 characters long.");
        }

        return match raw_key.from_base58()
        {
            Ok(bytes) =>
            {
                if bytes.len() != 32
                {
                    Err(format!("Decoded data should be 32 bytes, not {}", bytes.len()))?
                }

                Self::from_slice(&bytes)
            }
            Err(e) =>
            {
                Err(format!("Error decoding base58: {:?}", e))?
            }
        }
    }

    pub fn from_base64_private_key(raw_key: &str) -> Result<LegacyBitcoinAddressPair, Box<dyn std::error::Error>>
    {
        if raw_key.len() != 44
        {
            return Err("Invalid base64 private key! The encoded key string must be 44 characters long.")?;
        }

        let bytes = general_purpose::STANDARD.decode(raw_key)?;

        if bytes.len() != 32
        {
            return Err(format!("Decoded array should be 32 bytes, not {}", bytes.len()))?;
        }

        return Self::from_slice(&bytes);
    }

    pub fn from_slice(data: &[u8]) -> Result<LegacyBitcoinAddressPair, Box<dyn std::error::Error>>
    {
        return Ok(LegacyBitcoinAddressPair
        {
            compressed: LegacyBitcoinAddress::from_slice(&data, true)?,
            uncompressed: LegacyBitcoinAddress::from_slice(&data, false)?,
        });
    }

    pub fn from_mini_private_key(mini_key: &str) -> Result<LegacyBitcoinAddressPair, Box<dyn std::error::Error>>
    {
        return if let Some(pk) = is_valid_mini_key(mini_key)
        {
            LegacyBitcoinAddressPair::from_slice(&pk)
        }
        else
        {
            Err("Invalid mini key!")?
        }
    }

    pub fn uncompressed(&self) -> LegacyBitcoinAddress
    {
        return self.uncompressed.clone();
    }

    pub fn compressed(&self) -> LegacyBitcoinAddress
    {
        return self.compressed.clone();
    }
}

impl Display for LegacyBitcoinAddressPair
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "{} {}", self.compressed, self.uncompressed)
    }
}

impl Debug for LegacyBitcoinAddressPair
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result
    {
        return write!(f, "compressed: {{ {} }}, uncompressed: {{ {} }}", &self.compressed, &self.uncompressed);
    }
}

// ------------------------------------------------------------------------------------------------
// P2SH Segwit Bitcoin address.
// ------------------------------------------------------------------------------------------------

#[derive(Clone)]
pub struct LegacySegWitBitcoinAddress
{
    address: String,
    public_key_hash: String,
    wif_private_key: String,
    descriptor: String,
}

impl PartialEq<Self> for LegacySegWitBitcoinAddress
{
    fn eq(&self, other: &Self) -> bool
    {
        return self.address.eq(&other.address);
    }
}

impl PartialEq<&str> for LegacySegWitBitcoinAddress
{
    fn eq(&self, other: &&str) -> bool
    {
        return self.address.as_str().eq(*other);
    }
}

impl PartialEq<String> for LegacySegWitBitcoinAddress
{
    fn eq(&self, other: &String) -> bool
    {
        return self.address.eq(other);
    }
}

impl Eq for LegacySegWitBitcoinAddress
{

}

impl BitKeyWalletAddress for LegacySegWitBitcoinAddress
{
    fn from_wif_private_key(wif: &str) -> Result<LegacySegWitBitcoinAddress, Box<dyn std::error::Error>>
    {
        let secp = Secp256k1::new();
        let privk = PrivateKey::from_wif(&wif)?;

        let pubkey = privk.public_key(&secp);
        let pubkhash = pubkey.pubkey_hash();

        return Ok(LegacySegWitBitcoinAddress
        {
            address: Address::new(Network::Bitcoin, Payload::p2shwpkh(&pubkey)?).to_string(),
            public_key_hash: pubkhash.to_string(),
            wif_private_key: privk.to_wif(),
            descriptor: build_bitcoin_descriptor("p2shwpkh", privk.to_wif().as_str())?,
        });
    }

    fn wif_private_key(&self) -> &str
    {
        return self.wif_private_key.as_str();
    }

    fn from_base2_private_key(raw_key: &str, compressed: bool) -> Result<LegacySegWitBitcoinAddress, Box<dyn std::error::Error>>
    {
        let regex = Regex::new(r"[01]{8}")?;
        let octets: Vec<&str> = regex.find_iter(raw_key).map(|m| m.as_str()).collect();
        if octets.len() > 32
        {
            return Err("Invalid base2 binary key! The key cannot be longer than 32 bytes.")?;
        }

        let mut bytes: [u8; 32] = [0; 32];
        for (i, bs) in octets.iter().enumerate()
        {
            if let Some(bv) = crate::bin2octet(bs)
            {
                bytes[i] = bv;
            }
            else
            {
                return Err("Invalid base2 value!")?;
            }
        }

        return Self::from_slice(&bytes, compressed);
    }

    fn from_base16_private_key(raw_key: &str, compressed: bool) -> Result<LegacySegWitBitcoinAddress, Box<dyn std::error::Error>>
    {
        let hex_str: String;
        if raw_key.len() > 64
        {
            return Err("Invalid hexadecimal private key! The key cannot be longer than 64 characters (32 bytes).")?;
        }
        else if raw_key.len() == 64
        {
            hex_str = String::from(raw_key);
        }
        else
        {
            hex_str = format!("{:0<64}", raw_key);
        }

        let bytes = <[u8; 32]>::from_hex(hex_str.as_str())?;

        return Self::from_slice(&bytes, compressed);
    }

    fn from_base58_private_key(raw_key: &str, compressed: bool) -> Result<LegacySegWitBitcoinAddress, Box<dyn std::error::Error>>
    {
        if raw_key.len() != 44
        {
            panic!("Invalid base58 private key! The encoded key string must be 44 characters long.");
        }

        return match raw_key.from_base58()
        {
            Ok(bytes) =>
                {
                    if bytes.len() != 32
                    {
                        Err(format!("Decoded data should be 32 bytes, not {}", bytes.len()))?
                    }

                    Self::from_slice(&bytes, compressed)
                }
            Err(e) =>
                {
                    Err(format!("Error decoding base58: {:?}", e))?
                }
        }
    }

    fn from_base64_private_key(raw_key: &str, compressed: bool) -> Result<LegacySegWitBitcoinAddress, Box<dyn std::error::Error>>
    {
        if raw_key.len() != 44
        {
            return Err("Invalid base64 private key! The encoded key string must be 44 characters long.")?;
        }

        let bytes = general_purpose::STANDARD.decode(raw_key)?;

        if bytes.len() != 32
        {
            return Err(format!("Decoded array should be 32 bytes, not {}", bytes.len()))?;
        }

        return Self::from_slice(&bytes, compressed);
    }

    fn from_big_uint(key: &BigUint, _compressed: bool) -> Result<LegacySegWitBitcoinAddress, Box<dyn std::error::Error>>
    {
        let secp = Secp256k1::new();
        let priv_key_str = format!("{:0>64}", key.to_str_radix(16));
        let sk = SecretKey::from_str(&priv_key_str)?;
        let privk = PrivateKey::new(sk, Network::Bitcoin);

        let pubkey = privk.public_key(&secp);
        let pubkhash = pubkey.pubkey_hash();

        return Ok(LegacySegWitBitcoinAddress
        {
            address: Address::new(Network::Bitcoin, Payload::p2shwpkh(&pubkey)?).to_string(),
            public_key_hash: pubkhash.to_string(),
            wif_private_key: privk.to_wif(),
            descriptor: build_bitcoin_descriptor("p2shwpkh", privk.to_wif().as_str())?,
        });
    }

    fn from_slice(data: &[u8], _compressed: bool) -> Result<LegacySegWitBitcoinAddress, Box<dyn std::error::Error>>
    {
        let secp = Secp256k1::new();
        let sk = SecretKey::from_slice(data)?;
        let privk = PrivateKey::new(sk, Network::Bitcoin);

        let pubkey = privk.public_key(&secp);
        let pubkhash = pubkey.pubkey_hash();

        return Ok(LegacySegWitBitcoinAddress
        {
            address: Address::new(Network::Bitcoin, Payload::p2shwpkh(&pubkey)?).to_string(),
            public_key_hash: pubkhash.to_string(),
            wif_private_key: privk.to_wif(),
            descriptor: build_bitcoin_descriptor("p2shwpkh", privk.to_wif().as_str())?,
        });
    }
}

impl CryptocoinAddress for LegacySegWitBitcoinAddress
{
    fn descriptor(&self) -> &str
    {
        return self.descriptor.as_str();
    }
}

impl LegacySegWitBitcoinAddress
{
    fn from_private_key(key: &PrivateKey) -> Self
    {
        let secp = Secp256k1::new();

        let pubkey = key.public_key(&secp);
        let pubkhash = pubkey.pubkey_hash();

        return LegacySegWitBitcoinAddress
        {
            address: Address::new(Network::Bitcoin, Payload::p2shwpkh(&pubkey).unwrap()).to_string(),
            public_key_hash: pubkhash.to_string(),
            wif_private_key: key.to_wif(),
            descriptor: build_bitcoin_descriptor("p2shwpkh", key.to_wif().as_str()).unwrap(),
        }
    }
    pub fn public_key_hash(&self) -> &str
    {
        return self.public_key_hash.as_str();
    }
}

impl Display for LegacySegWitBitcoinAddress
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result
    {
        return write!(f, "{}", self.address);
    }
}

impl Debug for LegacySegWitBitcoinAddress
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result
    {
        return write!(f, "Address = {}, WIF private key = {}", self.address, self.wif_private_key);
    }
}

impl<Idx> std::ops::Index<Idx> for LegacySegWitBitcoinAddress where Idx: std::slice::SliceIndex<str>,
{
    type Output = Idx::Output;

    fn index(&self, index: Idx) -> &Self::Output
    {
        return &self.address.as_str()[index];
    }
}
    
impl AsRef<str> for LegacySegWitBitcoinAddress
{
    fn as_ref(&self) -> &str
    {
        return self.address.as_str();
    }
}

impl AsRef<[u8]> for LegacySegWitBitcoinAddress
{
    fn as_ref(&self) -> &[u8]
    {
        return self.address.as_bytes();
    }
}

// ------------------------------------------------------------------------------------------------
// Bech32 Segwit Bitcoin address.
// ------------------------------------------------------------------------------------------------

#[derive(Clone)]
pub struct Bech32SegWitBitcoinAddress
{
    address: String,
    public_key_hash: String,
    wif_private_key: String,
    descriptor: String,
}

impl PartialEq<Self> for Bech32SegWitBitcoinAddress
{
    fn eq(&self, other: &Self) -> bool
    {
        return self.address.eq(&other.address);
    }
}

impl PartialEq<&str> for Bech32SegWitBitcoinAddress
{
    fn eq(&self, other: &&str) -> bool
    {
        return self.address.as_str().eq(*other);
    }
}

impl PartialEq<String> for Bech32SegWitBitcoinAddress
{
    fn eq(&self, other: &String) -> bool
    {
        return self.address.eq(other);
    }
}

impl Eq for Bech32SegWitBitcoinAddress
{
    
}

impl BitKeyWalletAddress for Bech32SegWitBitcoinAddress
{
    fn from_wif_private_key(wif: &str) -> Result<Bech32SegWitBitcoinAddress, Box<dyn std::error::Error>>
    {
        let secp = Secp256k1::new();
        let privk = PrivateKey::from_wif(&wif)?;

        let pubkey = privk.public_key(&secp);
        let pubkhash = pubkey.pubkey_hash();

        return Ok(Bech32SegWitBitcoinAddress
        {
            address: Address::new(Network::Bitcoin, Payload::p2wpkh(&pubkey)?).to_string(),
            public_key_hash: pubkhash.to_string(),
            wif_private_key: privk.to_wif(),
            descriptor: build_bitcoin_descriptor("p2wpkh", privk.to_wif().as_str())?,
        });
    }

    fn wif_private_key(&self) -> &str
    {
        return self.wif_private_key.as_str();
    }

    fn from_base2_private_key(raw_key: &str, compressed: bool) -> Result<Bech32SegWitBitcoinAddress, Box<dyn std::error::Error>>
    {
        let regex = Regex::new(r"[01]{8}")?;
        let octets: Vec<&str> = regex.find_iter(raw_key).map(|m| m.as_str()).collect();
        if octets.len() > 32
        {
            return Err("Invalid base2 binary key! The key cannot be longer than 32 bytes.")?;
        }

        let mut bytes: [u8; 32] = [0; 32];
        for (i, bs) in octets.iter().enumerate()
        {
            if let Some(bv) = crate::bin2octet(bs)
            {
                bytes[i] = bv;
            }
            else
            {
                return Err("Invalid base2 value!")?;
            }
        }

        return Self::from_slice(&bytes, compressed);
    }

    fn from_base16_private_key(raw_key: &str, compressed: bool) -> Result<Bech32SegWitBitcoinAddress, Box<dyn std::error::Error>>
    {
        let hex_str: String;
        if raw_key.len() > 64
        {
            return Err("Invalid hexadecimal private key! The key cannot be longer than 64 characters (32 bytes).")?;
        }
        else if raw_key.len() == 64
        {
            hex_str = String::from(raw_key);
        }
        else
        {
            hex_str = format!("{:0<64}", raw_key);
        }

        let bytes = <[u8; 32]>::from_hex(hex_str.as_str())?;

        return Self::from_slice(&bytes, compressed);
    }

    fn from_base58_private_key(raw_key: &str, compressed: bool) -> Result<Bech32SegWitBitcoinAddress, Box<dyn std::error::Error>>
    {
        if raw_key.len() != 44
        {
            panic!("Invalid base58 private key! The encoded key string must be 44 characters long.");
        }

        return match raw_key.from_base58()
        {
            Ok(bytes) =>
                {
                    if bytes.len() != 32
                    {
                        Err(format!("Decoded data should be 32 bytes, not {}", bytes.len()))?
                    }

                    Self::from_slice(&bytes, compressed)
                }
            Err(e) =>
                {
                    Err(format!("Error decoding base58: {:?}", e))?
                }
        }
    }

    fn from_base64_private_key(raw_key: &str, compressed: bool) -> Result<Bech32SegWitBitcoinAddress, Box<dyn std::error::Error>>
    {
        if raw_key.len() != 44
        {
            return Err("Invalid base64 private key! The encoded key string must be 44 characters long.")?;
        }

        let bytes = general_purpose::STANDARD.decode(raw_key)?;

        if bytes.len() != 32
        {
            return Err(format!("Decoded array should be 32 bytes, not {}", bytes.len()))?;
        }

        return Self::from_slice(&bytes, compressed);
    }

    fn from_big_uint(key: &BigUint, _compressed: bool) -> Result<Bech32SegWitBitcoinAddress, Box<dyn std::error::Error>>
    {
        let secp = Secp256k1::new();
        let priv_key_str = format!("{:0>64}", key.to_str_radix(16));
        let sk = SecretKey::from_str(&priv_key_str)?;
        let privk = PrivateKey::new(sk, Network::Bitcoin);

        let pubkey = privk.public_key(&secp);
        let pubkhash = pubkey.pubkey_hash();

        return Ok(Bech32SegWitBitcoinAddress
        {
            address: Address::new(Network::Bitcoin, Payload::p2wpkh(&pubkey)?).to_string(),
            public_key_hash: pubkhash.to_string(),
            wif_private_key: privk.to_wif(),
            descriptor: build_bitcoin_descriptor("p2wpkh", privk.to_wif().as_str())?,
        });
    }

    fn from_slice(data: &[u8], _compressed: bool) -> Result<Bech32SegWitBitcoinAddress, Box<dyn std::error::Error>>
    {
        let secp = Secp256k1::new();
        let sk = SecretKey::from_slice(data)?;
        let privk = PrivateKey::new(sk, Network::Bitcoin);

        let pubkey = privk.public_key(&secp);
        let pubkhash = pubkey.pubkey_hash();

        return Ok(Bech32SegWitBitcoinAddress
        {
            address: Address::new(Network::Bitcoin, Payload::p2wpkh(&pubkey)?).to_string(),
            public_key_hash: pubkhash.to_string(),
            wif_private_key: privk.to_wif(),
            descriptor: build_bitcoin_descriptor("p2wpkh", privk.to_wif().as_str())?,
        });
    }
}

impl CryptocoinAddress for  Bech32SegWitBitcoinAddress
{
    fn descriptor(&self) -> &str
    {
        return self.descriptor.as_str();
    }
}

impl Bech32SegWitBitcoinAddress
{
    fn from_private_key(key: &PrivateKey) -> Self
    {
        let secp = Secp256k1::new();

        let pubkey = key.public_key(&secp);
        let pubkhash = pubkey.pubkey_hash();

        return Bech32SegWitBitcoinAddress
        {
            address: Address::new(Network::Bitcoin, Payload::p2wpkh(&pubkey).unwrap()).to_string(),
            public_key_hash: pubkhash.to_string(),
            wif_private_key: key.to_wif(),
            descriptor: build_bitcoin_descriptor("p2wpkh", key.to_wif().as_str()).unwrap(),
        }
    }

    pub fn public_key_hash(&self) -> &str
    {
        return &self.public_key_hash;
    }
}

impl Display for Bech32SegWitBitcoinAddress
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result
    {
        return write!(f, "{}", self.address);
    }
}

impl Debug for Bech32SegWitBitcoinAddress
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result
    {
        return write!(f, "Address = {}, WIF private key = {}", self.address, self.wif_private_key);
    }
}

impl<Idx> std::ops::Index<Idx> for Bech32SegWitBitcoinAddress where Idx: std::slice::SliceIndex<str>,
{
    type Output = Idx::Output;

    fn index(&self, index: Idx) -> &Self::Output
    {
        return &self.address.as_str()[index];
    }
}
    
impl AsRef<str> for Bech32SegWitBitcoinAddress
{
    fn as_ref(&self) -> &str
    {
        return self.address.as_str();
    }
}

impl AsRef<[u8]> for Bech32SegWitBitcoinAddress
{
    fn as_ref(&self) -> &[u8]
    {
        return self.address.as_bytes();
    }
}

// ------------------------------------------------------------------------------------------------
// Taproot Bitcoin address.
// ------------------------------------------------------------------------------------------------

/*#[derive(Clone)]
pub struct TaprootBitcoinAddress
{
    address: String,
    wif_private_key: String,
    descriptor: String,
}

impl PartialEq<Self> for TaprootBitcoinAddress
{
    fn eq(&self, other: &Self) -> bool
    {
        return self.address.eq(&other.address);
    }
}

impl PartialEq<&str> for TaprootBitcoinAddress
{
    fn eq(&self, other: &&str) -> bool
    {
        return self.address.as_str().eq(*other);
    }
}

impl PartialEq<String> for TaprootBitcoinAddress
{
    fn eq(&self, other: &String) -> bool
    {
        return self.address.eq(other);
    }
}

impl Eq for TaprootBitcoinAddress
{
    
}

impl BitKeyWalletAddress for TaprootBitcoinAddress
{
    fn from_wif_private_key(wif: &str) -> Result<TaprootBitcoinAddress, Box<dyn std::error::Error>>
    {
        let secp = Secp256k1::new();
        let privk = PrivateKey::from_wif(&wif)?;
        let (pubkey, _) = privk.inner.x_only_public_key(&secp);

        return Ok(TaprootBitcoinAddress
        {
            address: Address::p2tr(&secp, pubkey, None, Network::Bitcoin).to_string(),
            wif_private_key: privk.to_wif(),
            descriptor: build_bitcoin_descriptor("taproot", privk.to_wif().as_str())?,
        });
    }

    fn wif_private_key(&self) -> &str
    {
        return self.wif_private_key.as_str();
    }

    pub fn from_base2_private_key(raw_key: &str, merkle_root: &str) -> Result<TaprootBitcoinAddress, Box<dyn std::error::Error>>
    {
        let regex = Regex::new(r"[01]{8}")?;
        let octets: Vec<&str> = regex.find_iter(raw_key).map(|m| m.as_str()).collect();
        if octets.len() > 32
        {
            return Err("Invalid base2 binary key! The key cannot be longer than 32 bytes.")?;
        }

        let mut bytes: [u8; 32] = [0; 32];
        for (i, bs) in octets.iter().enumerate()
        {
            if let Some(bv) = crate::bin2octet(bs)
            {
                bytes[i] = bv;
            }
            else
            {
                return Err("Invalid base2 value!")?;
            }
        }

        return Self::from_slice(&bytes, merkle_root);
    }

    pub fn from_base16_private_key(raw_key: &str, merkle_root: &str) -> Result<TaprootBitcoinAddress, Box<dyn std::error::Error>>
    {
        let hex_str: String;
        if raw_key.len() > 64
        {
            return Err("Invalid hexadecimal private key! The key cannot be longer than 64 characters (32 bytes).")?;
        }
        else if raw_key.len() == 64
        {
            hex_str = String::from(raw_key);
        }
        else
        {
            hex_str = format!("{:0<64}", raw_key);
        }

        let bytes = <[u8; 32]>::from_hex(hex_str.as_str())?;

        return Self::from_slice(&bytes, merkle_root);
    }

    pub fn from_base58_private_key(raw_key: &str, merkle_root: &str) -> Result<TaprootBitcoinAddress, Box<dyn std::error::Error>>
    {
        if raw_key.len() != 44
        {
            panic!("Invalid base58 private key! The encoded key string must be 44 characters long.");
        }

        return match raw_key.from_base58()
        {
            Ok(bytes) =>
                {
                    if bytes.len() != 32
                    {
                        Err(format!("Decoded data should be 32 bytes, not {}", bytes.len()))?
                    }

                    Self::from_slice(&bytes, merkle_root)
                }
            Err(e) =>
                {
                    Err(format!("Error decoding base58: {:?}", e))?
                }
        }
    }

    pub fn from_base64_private_key(raw_key: &str, merkle_root: &str) -> Result<TaprootBitcoinAddress, Box<dyn std::error::Error>>
    {
        if raw_key.len() != 44
        {
            return Err("Invalid base64 private key! The encoded key string must be 44 characters long.")?;
        }

        let bytes = general_purpose::STANDARD.decode(raw_key)?;

        if bytes.len() != 32
        {
            return Err(format!("Decoded array should be 32 bytes, not {}", bytes.len()))?;
        }

        return Self::from_slice(&bytes, merkle_root);
    }

    pub fn from_slice(data: &[u8], merkle_root: &str) -> Result<TaprootBitcoinAddress, Box<dyn std::error::Error>>
    {
        let secp = Secp256k1::new();
        let sk = SecretKey::from_slice(data)?;
        let privk = PrivateKey::new(sk, Network::Bitcoin);
        let (pubkey, _) = sk.x_only_public_key(&secp);

        let merkle_root = if !merkle_root.is_empty()
        {
            match TapNodeHash::from_str(merkle_root)
            {
                Ok(mr) => Some(mr),
                Err(e) => { return Err(e.to_string())?; }
            }
        }
        else
        {
            None
        };

        return Ok(TaprootBitcoinAddress
        {
            address: Address::p2tr(&secp, pubkey, merkle_root, Network::Bitcoin).to_string(),
            wif_private_key: privk.to_wif(),
            descriptor: build_bitcoin_descriptor("taproot", privk.to_wif().as_str())?,
        });
    }

    fn from_big_uint(key: &BigUint, merkle_root: &str, _compressed: bool) -> Result<TaprootBitcoinAddress, Box<dyn std::error::Error>>
    {
        let secp = Secp256k1::new();
        let priv_key_str = format!("{:0>64}", key.to_str_radix(16));
        let sk = SecretKey::from_str(&priv_key_str).unwrap();
        let privk = PrivateKey::new(sk, Network::Bitcoin);
        let (pubkey, _) = sk.x_only_public_key(&secp);

        let merkle_root = if !merkle_root.is_empty()
        {
            match TapNodeHash::from_str(merkle_root)
            {
                Ok(mr) => Some(mr),
                Err(e) => { return Err(e.to_string())?; }
            }
        }
        else
        {
            None
        };

        return Ok(TaprootBitcoinAddress
        {
            address: Address::p2tr(&secp, pubkey, merkle_root, Network::Bitcoin).to_string(),
            wif_private_key: privk.to_wif(),
            descriptor: build_bitcoin_descriptor("taproot", privk.to_wif().as_str())?,
        });
    }
}

impl CryptocoinAddress for TaprootBitcoinAddress
{
    fn descriptor(&self) -> &str
    {
        return self.descriptor.as_str();
    }
}

impl TaprootBitcoinAddress
{
    fn from_private_key(key: &PrivateKey) -> Self
    {
        let secp = Secp256k1::new();
        let (pubkey, _) = key.inner.x_only_public_key(&secp);

        return TaprootBitcoinAddress
        {
            address: Address::p2tr(&secp, pubkey, None, Network::Bitcoin).to_string(),
            wif_private_key: key.to_wif(),
            descriptor: build_bitcoin_descriptor("taproot", key.to_wif().as_str()).unwrap(),
        }
    }
}

impl Display for TaprootBitcoinAddress
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result
    {
        return write!(f, "{}", self.address);
    }
}

impl Debug for TaprootBitcoinAddress
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result
    {
        return write!(f, "Address = {}, WIF private key = {}", self.address, self.wif_private_key);
    }
}

impl<Idx> std::ops::Index<Idx> for TaprootBitcoinAddress where Idx: std::slice::SliceIndex<str>,
{
    type Output = Idx::Output;

    fn index(&self, index: Idx) -> &Self::Output
    {
        return &self.address.as_str()[index];
    }
}
    
impl AsRef<str> for TaprootBitcoinAddress
{
    fn as_ref(&self) -> &str
    {
        return self.address.as_str();
    }
}

impl AsRef<[u8]> for TaprootBitcoinAddress
{
    fn as_ref(&self) -> &[u8]
    {
        return self.address.as_bytes();
    }
} */

// Returns the private key if candidate is a valid Mini Private Key per rules described in
// Bitcoin Wiki article "Mini private key format".
pub(crate) fn is_valid_mini_key(candidate: &str) -> Option<[u8; 32]>
{
    if candidate.len() != 22 && candidate.len() != 26 && candidate.len() != 30
    {
        return None;
    }
    if !candidate.starts_with('S')
    {
        return None;
    }
    let mkre = Regex::new(r"^S[1-9A-HJ-NP-Za-km-z]{21,29}$").unwrap();
    if !mkre.is_match(candidate)
    {
        return None;
    }
    let mut test_string = String::from(candidate);
    test_string.push('?');
    let ahash = sha256(test_string.as_bytes());
    if ahash[0] == 0
    {
        return Some(sha256(candidate.as_bytes()));
    }

    return None;
}

pub(crate) fn create_mini_key<T: AsRef<[u8]>>(seed: T) -> Option<(String, [u8; 32])>
{
    if seed.as_ref().len() == 0
    {
        return None;
    }
    // flow:
    // 1. take SHA256 of seed to yield 32 bytes
    // 2. base58-encode those 32 bytes as though it were a regular private key. now we have 51 characters.
    // 3. remove all instances of the digit 1. (likely source of typos)
    // 4. Extract the first 29 characters and prepend with an 'S' to make 30 characters.
    // 4. test to see if it matches the typo check.  while it does not, increment and try again.
    let sha256ofseed = sha256(seed.as_ref());
    let asbase58 = sha256ofseed.to_base58().replace("1", "");
    let asbase58 = &asbase58.as_bytes()[0..=29];

    let mut chars: Vec<u8> = Vec::with_capacity(30);
    let mut charstest: Vec<u8> = Vec::with_capacity(31);
    chars.push(b'S');
    charstest.push(b'S');
    for b in asbase58
    {
        chars.push(*b);
        charstest.push(*b);
    }
    charstest.push(b'?');

    while sha256(charstest.as_slice())[0] != 0
    {
        // As long as key doesn't pass typo check, increment it.
        for i in (0..chars.len()).rev()
        {
            let c = chars[i];
            if c == b'9'
            {
                charstest[i] = b'A';
                chars[i] = b'A';
                break;
            }
            else if c == b'H'
            {
                charstest[i] = b'J';
                chars[i] = b'J';
                break;
            }
            else if c == b'N'
            {
                charstest[i] = b'P';
                chars[i] = b'P';
                break;
            }
            else if c == b'Z'
            {
                charstest[i] = b'a';
                chars[i] = b'a';
                break;
            }
            else if c == b'k'
            {
                charstest[i] = b'm';
                chars[i] = b'm';
                break;
            }
            else if c == b'z'
            {
                charstest[i] = b'2';
                chars[i] = b'2';
                // No break - let loop increment prior character.
            }
            else
            {
                let ci = c + 1;
                charstest[i] = ci;
                chars[i] = ci;
                break;
            }
        }
    }

    let outb = sha256(chars.as_slice());
    return if let Ok(outs) = String::from_utf8(chars)
    {
        Some((outs, outb))
    }
    else
    {
        None
    }
}

#[test]
fn bitcoin_mini_key_tests()
{
    assert_ne!(is_valid_mini_key("S6c56bnXQiBjk9mqSYE7ykVQ7NzrRy"), None);
    assert_ne!(create_mini_key("The quick brown fox jumped over the lazy dog."), None);
}