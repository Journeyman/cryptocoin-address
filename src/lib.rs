/* Copyright (C) 2023-2024 Chris Morrison
 *
 * This file is part of cryptocoin-address.
 *
 * cryptocoin-address is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * cryptocoin-address is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with cryptocoin-address. If not, see <https://www.gnu.org/licenses/>.
 */
extern crate core;

use num_bigint::BigUint;
use std::fmt::Write;

pub mod bitcoin;

pub trait BitKeyWalletAddress
{
    fn from_wif_private_key(wif: &str) -> Result<Self, Box<dyn std::error::Error>> where Self: Sized;
    fn wif_private_key(&self) -> &str;
    fn from_base2_private_key(raw_key: &str, compressed: bool) -> Result<Self, Box<dyn std::error::Error>> where Self: Sized;
    fn from_base16_private_key(raw_key: &str, compressed: bool) -> Result<Self, Box<dyn std::error::Error>> where Self: Sized;
    fn from_base58_private_key(raw_key: &str, compressed: bool) -> Result<Self, Box<dyn std::error::Error>> where Self: Sized;
    fn from_base64_private_key(raw_key: &str, compressed: bool) -> Result<Self, Box<dyn std::error::Error>> where Self: Sized;
    fn from_big_uint(key: &BigUint, compressed: bool) -> Result<Self, Box<dyn std::error::Error>> where Self: Sized;
    fn from_slice(data: &[u8], compressed: bool) -> Result<Self, Box<dyn std::error::Error>> where Self: Sized;
}

pub trait CryptocoinAddress: Clone + std::fmt::Display + Eq
{
    fn descriptor(&self) -> &str;
}

pub(crate) fn bin2octet(octet: &str) -> Option<u8>
{
    let mut outb: u8 = 0;
    if octet.is_empty() || octet.len() > 8
    {
        return None;
    }

    for (i, bit) in octet.chars().rev().enumerate()
    {
        let shifted = 1 << i;
        if bit == '1'
        {
            outb |= shifted;
            continue;
        }
        if bit == '0'
        {
            continue;
        }
        return None;
    }

    return Some(outb);
}

// Extracted from the Bitcoin Core source code and converted to rust by Chris Morrison

////////////////////////////////////////////////////////////////////////////
// Checksum                                                               //
////////////////////////////////////////////////////////////////////////////

// This section implements a checksum algorithm for descriptors with the
// following properties:
// * Mistakes in a descriptor string are measured in "symbol errors". The higher
//   the number of symbol errors, the harder it is to detect:
//   * An error substituting a character from 0123456789()[],'/*abcdefgh@:$%{} for
//     another in that set always counts as 1 symbol error.
//     * Note that hex encoded keys are covered by these characters. Xprvs and
//       xpubs use other characters too, but already have their own checksum
//       mechanism.
//     * Function names like "multi()" use other characters, but mistakes in
//       these would generally result in an unparsable descriptor.
//   * A case error always counts as 1 symbol error.
//   * Any other 1 character substitution error counts as 1 or 2 symbol errors.
// * Any 1 symbol error is always detected.
// * Any 2 or 3 symbol error in a descriptor of up to 49154 characters is always detected.
// * Any 4 symbol error in a descriptor of up to 507 characters is always detected.
// * Any 5 symbol error in a descriptor of up to 77 characters is always detected.
// * Is optimized to minimize the chance a 5 symbol error in a descriptor up to 387 characters is undetected
// * Random errors have a chance of 1 in 2**40 of being undetected.
//
// These properties are achieved by expanding every group of 3 (non checksum) characters into
// 4 GF(32) symbols, over which a cyclic code is defined.

/*
 * Interprets c as 8 groups of 5 bits which are the coefficients of a degree 8 polynomial over GF(32),
 * multiplies that polynomial by x, computes its remainder modulo a generator, and adds the constant term val.
 *
 * This generator is G(x) = x^8 + {30}x^7 + {23}x^6 + {15}x^5 + {14}x^4 + {10}x^3 + {6}x^2 + {12}x + {9}.
 * It is chosen to define a cyclic error detecting code which is selected by:
 * - Starting from all BCH codes over GF(32) of degree 8 and below, which by construction guarantee detecting
 *   3 errors in windows up to 19000 symbols.
 * - Taking all those generators, and for degree 7 ones, extend them to degree 8 by adding all degree-1 factors.
 * - Selecting just the set of generators that guarantee detecting 4 errors in a window of length 512.
 * - Selecting one of those with best worst-case behavior for 5 errors in windows of length up to 512.
 *
 * The generator and the constants to implement it can be verified using this Sage code:
 *   B = GF(2) # Binary field
 *   BP.<b> = B[] # Polynomials over the binary field
 *   F_mod = b**5 + b**3 + 1
 *   F.<f> = GF(32, modulus=F_mod, repr='int') # GF(32) definition
 *   FP.<x> = F[] # Polynomials over GF(32)
 *   E_mod = x**3 + x + F.fetch_int(8)
 *   E.<e> = F.extension(E_mod) # Extension field definition
 *   alpha = e**2743 # Choice of an element in extension field
 *   for p in divisors(E.order() - 1): # Verify alpha has order 32767.
 *       assert((alpha**p == 1) == (p % 32767 == 0))
 *   G = lcm([(alpha**i).minpoly() for i in [1056,1057,1058]] + [x + 1])
 *   print(G) # Print out the generator
 *   for i in [1,2,4,8,16]: # Print out {1,2,4,8,16}*(G mod x^8), packed in hex integers.
 *       v = 0
 *       for coef in reversed((F.fetch_int(i)*(G % x**8)).coefficients(sparse=True)):
 *           v = v*32 + coef.integer_representation()
 *       print("0x%x" % v)
 */

fn poly_mod(c: u64, val: u64) ->u64
{
    let c0: u8 = (c >> 35).try_into().unwrap();
    let mut cv: u64 = ((c & 0x7ffffffff) << 5) ^ val;
    if (c0 & 1) > 0 { cv ^= 0xf5dee51989; }
    if (c0 & 2) > 0{ cv ^= 0xa9fdca3312; }
    if (c0 & 4) > 0 { cv ^= 0x1bab10e32d; }
    if (c0 & 8) > 0{ cv ^= 0x3706b1677a; }
    if (c0 & 16) > 0 { cv ^= 0x644d626ffd; }

    return cv;
}

pub(crate) fn descriptor_checksum(span: &str) -> Result<String, Box<dyn std::error::Error>>
{
    /** A character set designed such that:
           *  - The most common 'unprotected' descriptor characters (hex, keypaths) are in the first group of 32.
           *  - Case errors cause an offset that's a multiple of 32.
           *  - As many alphabetic characters are in the same group (while following the above restrictions).
           *
           * If p(x) gives the position of a character c in this character set, every group of 3 characters
           * (a,b,c) is encoded as the 4 symbols (p(a) & 31, p(b) & 31, p(c) & 31, (p(a) / 32) + 3 * (p(b) / 32) + 9 * (p(c) / 32).
           * This means that changes that only affect the lower 5 bits of the position, or only the higher 2 bits, will just
           * affect a single symbol.
           *
           * As a result, within-group-of-32 errors count as 1 symbol, as do cross-group errors that don't affect
           * the position within the groups.
     */
    const INPUT_CHARSET: &str = "0123456789()[],'/*abcdefgh@:$%{}IJKLMNOPQRSTUVWXYZ&+-.;<=>?!^_|~ijklmnopqrstuvwxyzABCDEFGH`#\"\\ ";
    const CHECKSUM_CHARSET: &str = "qpzry9x8gf2tvdw0s3jn54khce6mua7l"; // The character set for the checksum itself (same as bech32).

    let mut c: u64 = 1;
    let mut cls = 0;
    let mut clscount = 0;
    for ch in span.chars()
    {
        let pos = match INPUT_CHARSET.find(ch)
        {
            Some(p) => p,
            None =>
                {
                    return Err("Input string empty!")?;
                },
        };

        c = poly_mod(c, (pos & 31).try_into()?); // Emit a symbol for the position inside the group, for every character.
        cls = cls * 3 + (pos >> 5); // Accumulate the group numbers
        clscount = clscount + 1;
        if clscount == 3
        {
            // Emit an extra symbol representing the group numbers, for every 3 characters.
            c = poly_mod(c, cls.try_into()?);
            cls = 0;
            clscount = 0;
        }
    }
    if clscount > 0
    {
        c = poly_mod(c, cls.try_into()?);
    }
    for _ in 0..8
    {
        c = poly_mod(c, 0); // Shift further to determine the checksum.
    }
    c ^= 1; // Prevent appending zeroes from not affecting the checksum.

    let mut outs = String::with_capacity(8);
    for j in 0..8
    {
        outs.push(CHECKSUM_CHARSET.chars().nth(((c >> (5 * (7 - j))) & 31).try_into()?).unwrap());
    }

    return Ok(outs);
}

pub(crate) fn build_bitcoin_descriptor(address_type: &str, data: &str) -> Result<String, Box<dyn std::error::Error>>
{
    let mut outs = String::new();
    
    match address_type
    {
        "p2pkh" => { write!(&mut outs, "pkh({})", data)?; }
        "p2wpkh" => { write!(&mut outs, "wpkh({})", data)?; }
        "p2shwpkh" => { write!(&mut outs, "sh(wpkh({}))", data)?; },
        "taproot" => { write!(&mut outs, "tr({})", data)?; }
        _ => { return Err("Invalid address type!")?; },
    }

    let checksum = descriptor_checksum(outs.as_str())?;
    
    outs.push('#');
    outs.push_str(checksum.as_str());
    
    return Ok(outs);
}