use cryptocoin_address::{bitcoin::LegacyBitcoinAddress, BitKeyWalletAddress, CryptocoinAddress};
use num_bigint::BigUint;
use std::str::FromStr;
use cryptocoin_address::bitcoin::{Bech32SegWitBitcoinAddress, LegacyBitcoinAddressPair, LegacySegWitBitcoinAddress};

// Legacy Bitcoin address tests.

#[test]
fn legacy_from_mini_private_key()
{
    let result1 = LegacyBitcoinAddress::from_mini_private_key("S6c56bnXQiBjk9mqSYE7ykVQ7NzrRy", true).expect("Failed to parse mini private key:");
    assert_eq!(result1, "1PZuicD1ACRfBuKEgp2XaJhVvnwpeETDyn");
    assert_eq!(result1.wif_private_key(), "KynNkPDfpqvbLrrisfbDB11nocUD3p1nwVWSSpWPCAEYc8sXfM3M");
    assert_eq!(result1.descriptor(), "pkh(KynNkPDfpqvbLrrisfbDB11nocUD3p1nwVWSSpWPCAEYc8sXfM3M)#hyrfjs4t");
    assert_eq!(result1.is_compressed(), true);
    
    let result2 = LegacyBitcoinAddress::from_mini_private_key("S6c56bnXQiBjk9mqSYE7ykVQ7NzrRy", false).expect("Failed to parse mini private key:");
    assert_eq!(result2, "1CciesT23BNionJeXrbxmjc7ywfiyM4oLW");
    assert_eq!(result2.wif_private_key(), "5JPy8Zg7z4P7RSLsiqcqyeAF1935zjNUdMxcDeVrtU1oarrgnB7");
    assert_eq!(result2.descriptor(), "pkh(5JPy8Zg7z4P7RSLsiqcqyeAF1935zjNUdMxcDeVrtU1oarrgnB7)#qh5nqvqm");
    assert_eq!(result2.is_compressed(), false);
}

#[test]
fn legacy_from_wif_key()
{
    let result1 = LegacyBitcoinAddress::from_wif_private_key("Kx6EWgKRJ2GZuXbrDquQPAE8MWZLLdsT4YYgQs4hdF7rRL4jGLHx").expect("Failed to parse WIF private key:");
    assert_eq!(result1, "13pRRXkGVC9mhUSiw6xkYkMi1EX91VvsBE");
    assert_eq!(&result1[0..10], "13pRRXkGVC");
    assert_eq!(result1.wif_private_key(), "Kx6EWgKRJ2GZuXbrDquQPAE8MWZLLdsT4YYgQs4hdF7rRL4jGLHx");
    assert_eq!(result1.descriptor(), "pkh(Kx6EWgKRJ2GZuXbrDquQPAE8MWZLLdsT4YYgQs4hdF7rRL4jGLHx)#p42g6qy9");
    assert_eq!(result1.is_compressed(), true);

    let result2 = LegacyBitcoinAddress::from_wif_private_key("5J1jV2CspMgKnS4N7zJJz8Xcej3Lngcu89WP53jXW4CXEGF9M3A").expect("Failed to parse WIF private key:");
    assert_eq!(result2, "1Nt6XLmq8k8noafGGFdfwue74uJTFu9vQC");
    assert_eq!(&result2[0..10], "1Nt6XLmq8k");
    assert_eq!(result2.wif_private_key(), "5J1jV2CspMgKnS4N7zJJz8Xcej3Lngcu89WP53jXW4CXEGF9M3A");
    assert_eq!(result2.descriptor(), "pkh(5J1jV2CspMgKnS4N7zJJz8Xcej3Lngcu89WP53jXW4CXEGF9M3A)#ysh6zhj9");
    assert_eq!(result2.is_compressed(), false);
}

#[test]
fn legacy_from_big_uint()
{
    let bui = BigUint::from_str("72265151234817681412722471769720775084932759992649776771386320222828244847444").expect("Failed to parse integer from string:");

    let result1 = LegacyBitcoinAddress::from_big_uint(&bui, true).expect("Failed to parse private key from big uint:");
    assert_eq!(result1, "126phWhXCs6otfuhD3c7339xmDi495hKNj");
    assert_eq!(&result1[0..10], "126phWhXCs");
    assert_eq!(result1.wif_private_key(), "L2aH9Fw9vxXKeoU83nWULA3mPGAyGr9C1DF3YjCnKppQ39fNz8mD");
    assert_eq!(result1.descriptor(), "pkh(L2aH9Fw9vxXKeoU83nWULA3mPGAyGr9C1DF3YjCnKppQ39fNz8mD)#8fagg2y4");
    assert_eq!(result1.is_compressed(), true);

    let result2 = LegacyBitcoinAddress::from_big_uint(&bui, false).expect("Failed to parse private key from big uint:");
    assert_eq!(result2, "14cFyAWVHvekzi9qF5ZPbXqM9zborBagyG");
    assert_eq!(&result2[0..10], "14cFyAWVHv");
    assert_eq!(result2.wif_private_key(), "5K2eeDSs4AQdrM3X9LNdYysvZEQZEZRxxnNJoA7J4p5rB2h361E");
    assert_eq!(result2.descriptor(), "pkh(5K2eeDSs4AQdrM3X9LNdYysvZEQZEZRxxnNJoA7J4p5rB2h361E)#tdmj8qz2");
    assert_eq!(result2.is_compressed(), false);
}

#[test]
fn legacy_from_base2_key()
{
    let result1 = LegacyBitcoinAddress::from_base2_private_key("1001000010011001001011001111011100001100111001100111101100001011011011011000101001010101101000111101000100110111111110110101001100111001101111101110100100010111000011100010110111110011101001111100010110001001011110000100101110111110101000001111101101111110", true).expect("Failed to parse binary private key!");
    assert_eq!(result1, "1MT9ftTfrhMz5dbD879Z7N4x3JKLYhZ23Q");
    assert_eq!(&result1[0..10], "1MT9ftTfrh");
    assert_eq!(result1.wif_private_key(), "L24nqjA5MdXvc8f5Cyoeinn5YQQpfWcorsxL6GZLzFZkGHQsuENC");
    assert_eq!(result1.descriptor(), "pkh(L24nqjA5MdXvc8f5Cyoeinn5YQQpfWcorsxL6GZLzFZkGHQsuENC)#q4m2elwk");
    assert_eq!(result1.is_compressed(), true);

    let result2 = LegacyBitcoinAddress::from_base2_private_key("1001000010011001001011001111011100001100111001100111101100001011011011011000101001010101101000111101000100110111111110110101001100111001101111101110100100010111000011100010110111110011101001111100010110001001011110000100101110111110101000001111101101111110", false).expect("Failed to parse binary private key!");
    assert_eq!(result2, "1PpKYorVuqREJiv66pKy5rGoXenizKsS8o");
    assert_eq!(&result2[0..10], "1PpKYorVuq");
    assert_eq!(result2.wif_private_key(), "5Juy9mtuC22ZzMK9Cn3QxEYWaPybpExh53wDwD8nJxQjRoiC616");
    assert_eq!(result2.descriptor(), "pkh(5Juy9mtuC22ZzMK9Cn3QxEYWaPybpExh53wDwD8nJxQjRoiC616)#xcyx6te0");
    assert_eq!(result2.is_compressed(), false);
}

#[test]
fn legacy_from_base16_key()
{
    let result1 = LegacyBitcoinAddress::from_base16_private_key("FBFFED11B84FE0F3BAAB22E15CDE0E34A7A7E475C314AEEC1299DE8056761FA7", true).expect("Failed to parse hex private key:");
    assert_eq!(result1, "1BZGefNRvnAaU2j41yxrHzCWBBN9nZCFej");
    assert_eq!(&result1[0..10], "1BZGefNRvn");
    assert_eq!(result1.wif_private_key(), "L5fZkDzx7Qih5WAKxs9EWBMSqiaYjzn6gNsUvhFCGkinhfpmdcMx");
    assert_eq!(result1.descriptor(), "pkh(L5fZkDzx7Qih5WAKxs9EWBMSqiaYjzn6gNsUvhFCGkinhfpmdcMx)#3m9ka4le");
    assert_eq!(result1.is_compressed(), true);

    let result2 = LegacyBitcoinAddress::from_base16_private_key("fbffed11b84fe0f3baab22e15cde0e34a7a7e475c314aeec1299de8056761fa7", false).expect("Failed to parse hex private key:");
    assert_eq!(result2, "1EPWiyjem6e2wRXVUAn2k49mCcA7t56iez");
    assert_eq!(&result2[0..10], "1EPWiyjem6");
    assert_eq!(result2.wif_private_key(), "5KjGaMcnZidBSVMh6QbVSBxPAgKvs52cGifAxDHoxgMo9ofcMTJ");
    assert_eq!(result2.descriptor(), "pkh(5KjGaMcnZidBSVMh6QbVSBxPAgKvs52cGifAxDHoxgMo9ofcMTJ)#50ndr7jj");
    assert_eq!(result2.is_compressed(), false);
}

#[test]
fn legacy_from_base58_key()
{
    let result1 = LegacyBitcoinAddress::from_base58_private_key("GkBZcS6V9h9Uo847gF4pGBhvFPiXn8BMEBGskyG5HJfy", true).expect("Failed to parse base58 private key:");
    assert_eq!(result1, "15F1BeSJ646CnEw1958Moz1QwdxrnauVou");
    assert_eq!(&result1[0..10], "15F1BeSJ64");
    assert_eq!(result1.wif_private_key(), "L54SzMSvgsRYBdo1cnGV7nxWNiRUkNApnCeLhru7qPvAAdGpSMGb");
    assert_eq!(result1.descriptor(), "pkh(L54SzMSvgsRYBdo1cnGV7nxWNiRUkNApnCeLhru7qPvAAdGpSMGb)#jw0u8ug6");
    assert_eq!(result1.is_compressed(), true);

    let result2 = LegacyBitcoinAddress::from_base58_private_key("GkBZcS6V9h9Uo847gF4pGBhvFPiXn8BMEBGskyG5HJfy", false).expect("Failed to parse base58 private key:");
    assert_eq!(result2, "15C7hGtfVAXp45SM8Kn3qS2XpGTuX9PKx3");
    assert_eq!(&result2[0..10], "15C7hGtfVA");
    assert_eq!(result2.wif_private_key(), "5KbK86zXk2184mazEYZ4CWSiYew2kowLSCFMKmqWeFFmDfGEwv6");
    assert_eq!(result2.descriptor(), "pkh(5KbK86zXk2184mazEYZ4CWSiYew2kowLSCFMKmqWeFFmDfGEwv6)#tr72za86");
    assert_eq!(result2.is_compressed(), false);
}

#[test]
fn legacy_from_base64_key()
{
    let result1 = LegacyBitcoinAddress::from_base64_private_key("HGrqVN1aTJMtsFYDXbJhWBTqFTYiIndQE3AE4knjSmY=", true).expect("Failed to parse base64 private key:");
    assert_eq!(result1, "14hCh6xbooFoDu6b5N91z176EzQZ5BBT3M");
    assert_eq!(&result1[0..10], "14hCh6xboo");
    assert_eq!(result1.wif_private_key(), "KxAx7cA2oFnocpW1CABiKzi7Hs81qtVgyEutKDhwuNa9gXakK994");
    assert_eq!(result1.descriptor(), "pkh(KxAx7cA2oFnocpW1CABiKzi7Hs81qtVgyEutKDhwuNa9gXakK994)#qkgu4x0f");
    assert_eq!(result1.is_compressed(), true);

    let result2 = LegacyBitcoinAddress::from_base64_private_key("HGrqVN1aTJMtsFYDXbJhWBTqFTYiIndQE3AE4knjSmY=", false).expect("Failed to parse base64 private key:");
    assert_eq!(result2, "16cMTBkE3QrS8QRTTsWi4eQg7w2cL5w2C6");
    assert_eq!(&result2[0..10], "16cMTBkE3Q");
    assert_eq!(result2.wif_private_key(), "5J2oUKqieDQyKzS9esZnca1hr8jNVJ73iHkFLzFiSTdxteNeSMJ");
    assert_eq!(result2.descriptor(), "pkh(5J2oUKqieDQyKzS9esZnca1hr8jNVJ73iHkFLzFiSTdxteNeSMJ)#9qhsk5l5");
    assert_eq!(result2.is_compressed(), false);
}

// Legacy pair tests

#[test]
fn legacy_pair_from_mini_private_key()
{
    let result1 = LegacyBitcoinAddressPair::from_mini_private_key("SFPsTPAm7dBz4oMP444HbESpRoSB2P").expect("Failed to parse mini private key:");
    assert_eq!(result1.compressed(), "171GsDPcKPTrwkANm25bGwqGq4nDHZiAU7");
    assert_eq!(result1.uncompressed(), "1NjptEBiB7izdEZd7khjZqBjvEa2JYfogr");
    assert_eq!(result1.compressed().wif_private_key(), "L5HkJrCBbT4EMvekZYqhE6jN5pEeBAH6XuzVDfMDdssrKJcsvv4M");
    assert_eq!(result1.uncompressed().wif_private_key(), "5KeKsiWGYNJx8uwKUmNTLHFan3YdyDyfQmdb2xpB7YTvmX1YqAT");
    assert_eq!(result1.compressed().descriptor(), "pkh(L5HkJrCBbT4EMvekZYqhE6jN5pEeBAH6XuzVDfMDdssrKJcsvv4M)#8m0hj0lz");
    assert_eq!(result1.uncompressed().descriptor(), "pkh(5KeKsiWGYNJx8uwKUmNTLHFan3YdyDyfQmdb2xpB7YTvmX1YqAT)#mkz8d9s2");
    assert_eq!(result1.compressed().is_compressed(), true);
    assert_eq!(result1.uncompressed().is_compressed(), false);
}

#[test]
fn legacy_pair_from_wif_key()
{
    let result1 = LegacyBitcoinAddressPair::from_wif_private_key("L5W25pS3CXfkM7KJ38PgUWqLCY3zro1Cr9ziu3TWKsA4kkvfH7uC").expect("Failed to parse WIF private key:");
    assert_eq!(result1.compressed(), "1GLzkw3sXuZe8mb7AgirTx4k6tPEnQLwwv");
    assert_eq!(result1.uncompressed(), "1D9NCm5DGXvrEM2VhR3TBc2REYa4yEW5L3");
    assert_eq!(result1.compressed().wif_private_key(), "L5W25pS3CXfkM7KJ38PgUWqLCY3zro1Cr9ziu3TWKsA4kkvfH7uC");
    assert_eq!(result1.uncompressed().wif_private_key(), "5Kh78tXbUzFDYHqYxJzFP6QBhDvhJAyEZS9YhLb6jnoTgPyEu6h");
    assert_eq!(result1.compressed().descriptor(), "pkh(L5W25pS3CXfkM7KJ38PgUWqLCY3zro1Cr9ziu3TWKsA4kkvfH7uC)#h2fwpq83");
    assert_eq!(result1.uncompressed().descriptor(), "pkh(5Kh78tXbUzFDYHqYxJzFP6QBhDvhJAyEZS9YhLb6jnoTgPyEu6h)#ed3u4ygl");
    assert_eq!(result1.compressed().is_compressed(), true);
    assert_eq!(result1.uncompressed().is_compressed(), false);

    let result2 = LegacyBitcoinAddressPair::from_wif_private_key("5Kh78tXbUzFDYHqYxJzFP6QBhDvhJAyEZS9YhLb6jnoTgPyEu6h").expect("Failed to parse WIF private key:");
    assert_eq!(result2.compressed(), "1GLzkw3sXuZe8mb7AgirTx4k6tPEnQLwwv");
    assert_eq!(result2.uncompressed(), "1D9NCm5DGXvrEM2VhR3TBc2REYa4yEW5L3");
    assert_eq!(result2.compressed().wif_private_key(), "L5W25pS3CXfkM7KJ38PgUWqLCY3zro1Cr9ziu3TWKsA4kkvfH7uC");
    assert_eq!(result2.uncompressed().wif_private_key(), "5Kh78tXbUzFDYHqYxJzFP6QBhDvhJAyEZS9YhLb6jnoTgPyEu6h");
    assert_eq!(result2.compressed().descriptor(), "pkh(L5W25pS3CXfkM7KJ38PgUWqLCY3zro1Cr9ziu3TWKsA4kkvfH7uC)#h2fwpq83");
    assert_eq!(result2.uncompressed().descriptor(), "pkh(5Kh78tXbUzFDYHqYxJzFP6QBhDvhJAyEZS9YhLb6jnoTgPyEu6h)#ed3u4ygl");
    assert_eq!(result2.compressed().is_compressed(), true);
    assert_eq!(result2.uncompressed().is_compressed(), false);
}

#[test]
fn legacy_pair_from_big_uint()
{
    let bui = BigUint::from_str("70690260355726641725016746261388483486473250907628428372357802887947762530808").expect("Failed to parse integer from string:");

    let result = LegacyBitcoinAddressPair::from_big_uint(&bui).expect("Failed to parse private key from big uint:");
    assert_eq!(result.compressed(), "1E3GHgXFUrXWGS2ySq1W4j1M9uyGmR3SaQ");
    assert_eq!(result.uncompressed(), "15NNRxqxPvzLxthdDBmnMPtqPX2hMZNLzW");
    assert_eq!(result.compressed().wif_private_key(), "L2TWakPxZRYYRu6UysaesErNf6wNEyb1MPifY6VhaXq8ugRioT6b");
    assert_eq!(result.uncompressed().wif_private_key(), "5K17hj1Db5exc6TcnxsDf7mGL4A9SNRvYoqQgfrYrHW2nzGkJfc");
    assert_eq!(result.compressed().descriptor(), "pkh(L2TWakPxZRYYRu6UysaesErNf6wNEyb1MPifY6VhaXq8ugRioT6b)#39txkfh2");
    assert_eq!(result.uncompressed().descriptor(), "pkh(5K17hj1Db5exc6TcnxsDf7mGL4A9SNRvYoqQgfrYrHW2nzGkJfc)#qpw7aff3");
    assert_eq!(result.compressed().is_compressed(), true);
    assert_eq!(result.uncompressed().is_compressed(), false);
}

#[test]
fn legacy_pair_from_base2_key()
{
    let result = LegacyBitcoinAddressPair::from_base2_private_key("0011000111001000111111001011011001110110111100010101100011011011100011110101111100110110111110100010101000011100011101101000001110010011100001100010010101110100111111000011110111001011110110010101000011010000010111001101010011011001001001010000001011111111").expect("Failed to parse binary private key!");
    assert_eq!(result.compressed(), "1MdVqb5fDj1cwxU3GTia5sPLJN6YMWsF7n");
    assert_eq!(result.uncompressed(), "1K3yPZmQwtdGXHwGY9oUhjFHSVyMo5B6pt");
    assert_eq!(result.compressed().wif_private_key(), "KxtVBFRcZSnsvUrpL1daMY1FQBYfL4gvotApV1P5pH3phRZXrppy");
    assert_eq!(result.uncompressed().wif_private_key(), "5JCDGv7UzeZbeVWs9PM4w3VbyugoJ1PBqEQXF3oh4TkEXVph1Go");
    assert_eq!(result.compressed().descriptor(), "pkh(KxtVBFRcZSnsvUrpL1daMY1FQBYfL4gvotApV1P5pH3phRZXrppy)#qalua644");
    assert_eq!(result.uncompressed().descriptor(), "pkh(5JCDGv7UzeZbeVWs9PM4w3VbyugoJ1PBqEQXF3oh4TkEXVph1Go)#3gfj4xka");
    assert_eq!(result.compressed().is_compressed(), true);
    assert_eq!(result.uncompressed().is_compressed(), false);
}

#[test]
fn legacy_pair_from_base16_key()
{
    let result = LegacyBitcoinAddressPair::from_base16_private_key("3C260D9AAA31F08DF3527D55FF3EF86B420ADE55483FE4AE6A126E49735C1DC0").expect("Failed to parse hex private key:");
    assert_eq!(result.compressed(), "1JF42VXqsnCGpTtQJdoUpDDnLKrEbNwoNt");
    assert_eq!(result.uncompressed(), "13pHUQMnmZR73s8BkrSoRX6fpC7rm4dsBA");
    assert_eq!(result.compressed().wif_private_key(), "KyEdcFzK9tervH7VorYMQ1mJ19d9dT51P3A1RXkHwaxd8YHvGTgS");
    assert_eq!(result.uncompressed().wif_private_key(), "5JGmzqFoFhhkUvwwZNCBWgbWEB7NtkyBxk5m3qeuKiE4Cn8Hrwh");
    assert_eq!(result.compressed().descriptor(), "pkh(KyEdcFzK9tervH7VorYMQ1mJ19d9dT51P3A1RXkHwaxd8YHvGTgS)#xfe640pw");
    assert_eq!(result.uncompressed().descriptor(), "pkh(5JGmzqFoFhhkUvwwZNCBWgbWEB7NtkyBxk5m3qeuKiE4Cn8Hrwh)#5jcce9hs");
    assert_eq!(result.compressed().is_compressed(), true);
    assert_eq!(result.uncompressed().is_compressed(), false);
}

#[test]
fn legacy_pair_from_base58_key()
{
    let result = LegacyBitcoinAddressPair::from_base58_private_key("412NseCcU1BEfU59RGhbUV1jhRGqQ4TYpMbr45gTX43g").expect("Failed to parse base58 private key:");
    assert_eq!(result.compressed(), "1HBNyKSAzAG5UGbby4GALAmsojME4PPVwN");
    assert_eq!(result.uncompressed(), "17m5MofWKfxsx5SKbBrd5T7VSFT2LeFF6W");
    assert_eq!(result.compressed().wif_private_key(), "KxiNPfX6WCg5CweLxjDQx1R8jz8JfCrZJ4PDqXC8oFubQGGj3kXx");
    assert_eq!(result.uncompressed().wif_private_key(), "5J9vLBYHet8VbRcebq5dEpfzwbhAthkQBD6VHRxzW4NtJ4MF2qw");
    assert_eq!(result.compressed().descriptor(), "pkh(KxiNPfX6WCg5CweLxjDQx1R8jz8JfCrZJ4PDqXC8oFubQGGj3kXx)#7crrznvv");
    assert_eq!(result.uncompressed().descriptor(), "pkh(5J9vLBYHet8VbRcebq5dEpfzwbhAthkQBD6VHRxzW4NtJ4MF2qw)#nr06hsj8");
    assert_eq!(result.compressed().is_compressed(), true);
    assert_eq!(result.uncompressed().is_compressed(), false);
}

#[test]
fn legacy_pair_from_base64_key()
{
    let result = LegacyBitcoinAddressPair::from_base64_private_key("eLC8/MkbdBJPIgFRE57wLgeWJ+iVXh0ONL3q764v9O8=").expect("Failed to parse base64 private key:");
    assert_eq!(result.compressed(), "1P6368gqdf2Y5rvnUawwv9ryKdP4j2snzh");
    assert_eq!(result.uncompressed(), "19zAv9oB1CKYQSTWeiYGTBtLKBZeg8W2ng");
    assert_eq!(result.compressed().wif_private_key(), "L1GKMNKhv71Bf6a3TG2efq9FHAZngjZDtqPvvMAHWDQLQ6eQcgmP");
    assert_eq!(result.uncompressed().wif_private_key(), "5JjSTN9KLHKAfYAkbx4YeLyF7P751PnN3gWfVrRm4uCdZt5hgt5");
    assert_eq!(result.compressed().descriptor(), "pkh(L1GKMNKhv71Bf6a3TG2efq9FHAZngjZDtqPvvMAHWDQLQ6eQcgmP)#cwtyjlfk");
    assert_eq!(result.uncompressed().descriptor(), "pkh(5JjSTN9KLHKAfYAkbx4YeLyF7P751PnN3gWfVrRm4uCdZt5hgt5)#8w086s4d");
    assert_eq!(result.compressed().is_compressed(), true);
    assert_eq!(result.uncompressed().is_compressed(), false);
}

// Legacy SegWit tests.

#[test]
fn legacy_segwit_from_wif_key()
{
    let result = LegacySegWitBitcoinAddress::from_wif_private_key("KyzmZvmAUPViXvZjSSwVwQ4EKFfRPu6FzNBDM5o3fyejs2TcVPQq").expect("Failed to parse WIF private key:");
    assert_eq!(result, "3MG89f5mNUuuqjqaB5nz6B1g8kkWaEJcfd");
    assert_eq!(&result[8..], "NUuuqjqaB5nz6B1g8kkWaEJcfd");
    assert_eq!(result.wif_private_key(), "KyzmZvmAUPViXvZjSSwVwQ4EKFfRPu6FzNBDM5o3fyejs2TcVPQq");
    assert_eq!(result.descriptor(), "sh(wpkh(KyzmZvmAUPViXvZjSSwVwQ4EKFfRPu6FzNBDM5o3fyejs2TcVPQq))#kn5aan28");
}

#[test]
fn legacy_segwit_from_big_uint()
{
    let bui = BigUint::from_str("110839320252927571904292116042899703365509300573470934757210542289701145886759").expect("Failed to parse integer from string:");

    let result = LegacySegWitBitcoinAddress::from_big_uint(&bui, true).expect("Failed to parse private key from big uint:");
    assert_eq!(result, "3BhcUm7akbWPv7d2Br63P9Xow2zZG8WPPV");
    assert_eq!(&result[8..], "kbWPv7d2Br63P9Xow2zZG8WPPV");
    assert_eq!(result.wif_private_key(), "L5S4Df6VtL3HEjrmw7mdYLTRyyG2UMcNYW6KqobQbBTW2EC5QG8A");
    assert_eq!(result.descriptor(), "sh(wpkh(L5S4Df6VtL3HEjrmw7mdYLTRyyG2UMcNYW6KqobQbBTW2EC5QG8A))#t8dlh8h7");
}

#[test]
fn legacy_segwit_from_base2_key()
{
    let result = LegacySegWitBitcoinAddress::from_base2_private_key("0110110001001010111110101011101010101010010001010101111100010001101100100001110100111100100000100010001010100101000000100000110001101101011101010101110111011010001111100100001110000101010100011010111101011110100110011110111010001110101100000010011000110111", true).expect("Failed to parse binary private key!");
    assert_eq!(result, "3HbMyD3qrmhz3pQLw66U5GFDga8Z9niLGm");
    assert_eq!(&result[8..], "rmhz3pQLw66U5GFDga8Z9niLGm");
    assert_eq!(result.wif_private_key(), "KzrDbxaHhEoEBp92i1PxVjAVgfVdAC8PDVBPMsyfs9QzCd4JXTZa");
    assert_eq!(result.descriptor(), "sh(wpkh(KzrDbxaHhEoEBp92i1PxVjAVgfVdAC8PDVBPMsyfs9QzCd4JXTZa))#xp25x9rx");
}

#[test]
fn legacy_segwit_from_base16_key()
{
    let result = LegacySegWitBitcoinAddress::from_base16_private_key("1AFAA8194CEBF75808C114EB0431BDC1E82B79DFD52A10A0B698247F1972123B", true).expect("Failed to parse hex private key:");
    assert_eq!(result, "34eV1gSpscU7PktZ38MvXyMjLqHooYG3S1");
    assert_eq!(&result[8..], "scU7PktZ38MvXyMjLqHooYG3S1");
    assert_eq!(result.wif_private_key(), "Kx89vw24iGN442HYJC2MfrERGjB7aiwiBx1o4HY7ppx6cffjPmLN");
    assert_eq!(result.descriptor(), "sh(wpkh(Kx89vw24iGN442HYJC2MfrERGjB7aiwiBx1o4HY7ppx6cffjPmLN))#fcvt05ht");
}

#[test]
fn legacy_segwit_from_base58_key()
{
    let result = LegacySegWitBitcoinAddress::from_base58_private_key("4gfAXPpjQEAn2CpD31EEvNTzT222ngjzNhSNQDZdrb6j", true).expect("Failed to parse base58 private key:");
    assert_eq!(result, "3NJBxxeWa5QgJFwKkShRctr1Ygifjk2pH9");
    assert_eq!(&result[8..], "a5QgJFwKkShRctr1Ygifjk2pH9");
    assert_eq!(result.wif_private_key(), "Ky477wJFe6rdfmEDE6xnKNuZgB7ExKcPi5S4nrNPm4L8YT7RDKy6");
    assert_eq!(result.descriptor(), "sh(wpkh(Ky477wJFe6rdfmEDE6xnKNuZgB7ExKcPi5S4nrNPm4L8YT7RDKy6))#xpkdj5pf");
}

#[test]
fn legacy_segwit_from_base64_key()
{
    let result = LegacySegWitBitcoinAddress::from_base64_private_key("rfBU+TnqLL19sZqix0WJeeQY+Ol/jww/PIjw1131+bA=", true).expect("Failed to parse base64 private key:");
    assert_eq!(result, "3HhUa2z8K2j5zq3KTr98KiRnimLNqDVGEy");
    assert_eq!(&result[8..], "K2j5zq3KTr98KiRnimLNqDVGEy");
    assert_eq!(result.wif_private_key(), "L33pp8ib45C4fK8e5K3WnTv7eHeBtidKD7WEZba3UoTAtJzBWF4H");
    assert_eq!(result.descriptor(), "sh(wpkh(L33pp8ib45C4fK8e5K3WnTv7eHeBtidKD7WEZba3UoTAtJzBWF4H))#85hdvrkl");
}

// Bech32 Segwit tests.

#[test]
fn bech32_from_wif_key()
{
    let result = Bech32SegWitBitcoinAddress::from_wif_private_key("KziJAsjV4kQVYaKqWeUgTfZa65Sdoyi77biHWE4oSr4ZutGKLF8E").expect("Failed to parse WIF private key:");
    assert_eq!(result, "bc1qe3n4r8tscwgeu42ercuc3umatl0s4dk55zhphw");
    assert_eq!(&result[8..12], "r8ts");
    assert_eq!(result.wif_private_key(), "KziJAsjV4kQVYaKqWeUgTfZa65Sdoyi77biHWE4oSr4ZutGKLF8E");
    assert_eq!(result.descriptor(), "wpkh(KziJAsjV4kQVYaKqWeUgTfZa65Sdoyi77biHWE4oSr4ZutGKLF8E)#cny0zlmr");
}

#[test]
fn bech32_from_big_uint()
{
    let bui = BigUint::from_str("82826172227603858855694814722120092092367617382027183751566903284866761319571").expect("Failed to parse integer from string:");

    let result = Bech32SegWitBitcoinAddress::from_big_uint(&bui, true).expect("Failed to parse private key from big uint:");
    assert_eq!(result, "bc1qwym2vqjkzy6sce7xnh97me6f83ks7lzaj33nj9");
    assert_eq!(&result[8..12], "vqjk");
    assert_eq!(result.wif_private_key(), "L3Mfc2gsQozEWh83eu4JpJTkWvhznSZTKA3zCPTFhZDkb5qGEwDy");
    assert_eq!(result.descriptor(), "wpkh(L3Mfc2gsQozEWh83eu4JpJTkWvhznSZTKA3zCPTFhZDkb5qGEwDy)#5sx42s9n");
}

#[test]
fn bech32_from_base2_key()
{
    let result = Bech32SegWitBitcoinAddress::from_base2_private_key("1110100110001001010111001001111111010010010011100000111101100001011111000000000111100110111010011111110100001001010011000010010010111011000000001000001101001001100001101011110101011101101001110011000110110111100101011010010000001110001001101001011010101000", true).expect("Failed to parse binary private key!");
    assert_eq!(result, "bc1qk6hqmlhalda3rx0csrtxvvkx0mka5mk3hug2px");
    assert_eq!(&result[8..12], "mlha");
    assert_eq!(result.wif_private_key(), "L53g8V6UHGzmNrKbmySDmyEyHXbYponEkUPRrGeLVFFaMrENGdW2");
    assert_eq!(result.descriptor(), "wpkh(L53g8V6UHGzmNrKbmySDmyEyHXbYponEkUPRrGeLVFFaMrENGdW2)#wfcyrh06");
}

#[test]
fn bech32_from_base16_key()
{
    let result = Bech32SegWitBitcoinAddress::from_base16_private_key("ADD69090C67AED9257EC819F91FC63A18BE778693936296FA7981EB10FC8C684", true).expect("Failed to parse hex private key:");
    assert_eq!(result, "bc1qtl9g9f7mwq7zuajzczrxpuglh4jvhp5rewv6wj");
    assert_eq!(&result[8..12], "9f7m");
    assert_eq!(result.wif_private_key(), "L33dTwkipVTPUmyZSAnVbx2RvmYHNgZbjVcBHUZhVJ7wxPxtFTAZ");
    assert_eq!(result.descriptor(), "wpkh(L33dTwkipVTPUmyZSAnVbx2RvmYHNgZbjVcBHUZhVJ7wxPxtFTAZ)#mfslarls");
}

#[test]
fn bech32_from_base58()
{
    let result = Bech32SegWitBitcoinAddress::from_base58_private_key("33jZD16uknjhr7zKtLnZgSVtGc38Dc2bAV38s3bNDxBK", true).expect("Failed to parse base58 private key:");
    assert_eq!(result, "bc1q6nrsxzjuxeed8v4xepphk74r3dqrrqzjr0jauc");
    assert_eq!(&result[8..12], "xzju");
    assert_eq!(result.wif_private_key(), "KxEqVFmdsSgw6zwMM1dMEBEwNWHxXYuEugBU1MxXJv3BEAYtrfTA");
    assert_eq!(result.descriptor(), "wpkh(KxEqVFmdsSgw6zwMM1dMEBEwNWHxXYuEugBU1MxXJv3BEAYtrfTA)#7lfz928n");
}

#[test]
fn bech32_from_base64()
{
    let result = Bech32SegWitBitcoinAddress::from_base64_private_key("ka/XEYATr289gAGbB4kyy7vvv2vvbu/sLs3/+vfAdho=", true).expect("Failed to parse base64 private key:");
    assert_eq!(result, "bc1q92r9c8lklquqveayp0fclkc3hnccwvfua2jlnf");
    assert_eq!(&result[8..12], "c8lk");
    assert_eq!(result.wif_private_key(), "L26uZqXNaGS2zueQLTQK4wwHWg1HdX9jJtf9AS1G3Hpwcjzdb57J");
    assert_eq!(result.descriptor(), "wpkh(L26uZqXNaGS2zueQLTQK4wwHWg1HdX9jJtf9AS1G3Hpwcjzdb57J)#xnxc4sag");
}

// Taproot tests.

/*#[test]
fn taproot_from_wif_key()
{
    let result = TaprootBitcoinAddress::from_wif_private_key("KzcRfEWmifchoc6kp1PnEBcfiknStypfmUxqsG3WopZvv8LYaXau").expect("Failed to parse WIF private key:");
    assert_eq!(result, "bc1pppntuatqzqzmennn5uhf9elfyn7kd6cvvdhns3lwm8wccmjfed8shym3ml");
    assert_eq!(&result[..61], "bc1pppntuatqzqzmennn5uhf9elfyn7kd6cvvdhns3lwm8wccmjfed8shym3m");
    assert_eq!(result.wif_private_key(), "KzcRfEWmifchoc6kp1PnEBcfiknStypfmUxqsG3WopZvv8LYaXau");
    assert_eq!(result.descriptor(), "tr(KzcRfEWmifchoc6kp1PnEBcfiknStypfmUxqsG3WopZvv8LYaXau)#2vkr277u");
}

#[test]
fn taproot_from_big_uint()
{
    let bui = BigUint::from_str("42459907000479013278978829135360611927995359804039340692970157952197311377267").expect("Failed to parse integer from string:");
    let result = TaprootBitcoinAddress::from_big_uint(&bui, "").expect("Failed to parse private key from big uint:");
    assert_eq!(result, "bc1ppq3f6xz3xfngq3u4x89rqwlethlme5fevzy2lhj6aq8lgf69gawq5490e9");
    assert_eq!(&result[..61], "bc1ppq3f6xz3xfngq3u4x89rqwlethlme5fevzy2lhj6aq8lgf69gawq5490e");
    assert_eq!(result.wif_private_key(), "KzNBpwXFk5uXuhxsThz2cYaVfUhHutrfmZDHTeC3r852nEdnRcPY");
    assert_eq!(result.descriptor(), "tr(KzNBpwXFk5uXuhxsThz2cYaVfUhHutrfmZDHTeC3r852nEdnRcPY)#dp74prav");
}

#[test]
fn taproot_from_base2_key()
{
    let result = TaprootBitcoinAddress::from_base2_private_key("0000110110100010110001000111010011100110110000010111001101100000101101010011110110100111010100010011101010111111000001100111100011000101010110100100010101110000011111110100001001010000110111101000100101100111111101111111111111101100111111110000101101001001", "").expect("Failed to parse binary private key!");
    assert_eq!(result, "bc1ppqw6m76expkdnhlfgggflrkagg76uj0f7k6hgm0pg97dxf3fat3sp0smuk");
    assert_eq!(&result[..61], "bc1ppqw6m76expkdnhlfgggflrkagg76uj0f7k6hgm0pg97dxf3fat3sp0smu");
    assert_eq!(result.wif_private_key(), "KwgDYdHgTK37xgbNELudX5bh3fEPmkjp51ciNAC2zmfzMk27XajB");
    assert_eq!(result.descriptor(), "tr(KwgDYdHgTK37xgbNELudX5bh3fEPmkjp51ciNAC2zmfzMk27XajB)#lmusue2t");
}

#[test]
fn taproot_from_base16_key()
{
    let result = TaprootBitcoinAddress::from_base16_private_key("f9dc4b1e496de10a59310ed069240f39c7f762e0f2d87240382b6189911b02b8", "").expect("Failed to parse hex private key:");
    assert_eq!(result, "bc1ppl5hc3tl9e0jsghfwxguqkqedy4jyj47uuz2df9vpdj27uljlf2szp97k5");
    assert_eq!(&result[..61], "bc1ppl5hc3tl9e0jsghfwxguqkqedy4jyj47uuz2df9vpdj27uljlf2szp97k");
    assert_eq!(result.wif_private_key(), "L5bQZgfPqTxAWe5M4f4npU55fh7AcLNRqMJbQbUKU2btL4zCYaW1");
    assert_eq!(result.descriptor(), "tr(L5bQZgfPqTxAWe5M4f4npU55fh7AcLNRqMJbQbUKU2btL4zCYaW1)#gy5asej0");
}

#[test]
fn taproot_from_base58()
{
    let result = TaprootBitcoinAddress::from_base58_private_key("DMb2CroddwbZo8ofvE2bCTaT9NmEd7NBYohEENpsvXom", "").expect("Failed to parse base58 private key:");
    assert_eq!(result, "bc1pq5l2s8vumyrelk9jkputkq8xj4y594lkna0r66p4wug6y3k2kj5qky0r6c");
    assert_eq!(&result[..61], "bc1pq5l2s8vumyrelk9jkputkq8xj4y594lkna0r66p4wug6y3k2kj5qky0r6");
    assert_eq!(result.wif_private_key(), "L3NYrfGdajFtx9FuFeiKBsgEFSZAbnaLdpPgF9Z1go7o9dTQg42H");
    assert_eq!(result.descriptor(), "tr(L3NYrfGdajFtx9FuFeiKBsgEFSZAbnaLdpPgF9Z1go7o9dTQg42H)#ksusadlq");
}

#[test]
fn taproot_from_base64()
{
    let result = TaprootBitcoinAddress::from_base64_private_key("qgLXufyEGqHEKzw0V0EG2Wk8Qmv5MoyaTXp78ljsxWI=", "").expect("Failed to parse base64 private key:");
    assert_eq!(result, "bc1puh4vr8zdtwn5zdmq0c7watzn5y3dfc6rqq8fgah35g692ftp47csdryyd8");
    assert_eq!(&result[..61], "bc1puh4vr8zdtwn5zdmq0c7watzn5y3dfc6rqq8fgah35g692ftp47csdryyd");
    assert_eq!(result.wif_private_key(), "L2vBzG4rdMHLi5DHaf8sAgwgDcMTugBjAboatNR7a42PgHAAMcac");
    assert_eq!(result.descriptor(), "tr(L2vBzG4rdMHLi5DHaf8sAgwgDcMTugBjAboatNR7a42PgHAAMcac)#rmnpuwrj");
} */